package pl.mk.resteasy;

import io.undertow.servlet.api.DeploymentInfo;
import org.jboss.resteasy.plugins.server.undertow.UndertowJaxrsServer;
import org.jboss.resteasy.test.TestPortProvider;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;

/**
 * see: https://docs.jboss.org/resteasy/docs/3.6.3.Final/userguide/html/index.html
 * */
public class UndertowTest {
    private static UndertowJaxrsServer server;

    @BeforeClass
    public static void init() throws Exception {
        server = new UndertowJaxrsServer().start();
    }

    @AfterClass
    public static void stop() throws Exception {
        server.stop();
    }

    @Test
    public void testApplicationPath() throws Exception {
        server.deploy(SimpleRestApp.class);
        Client client = ClientBuilder.newClient();
        final String url = TestPortProvider.generateURL("/base/test");
        String val = client.target(url)
                .request().get(String.class);
        Assert.assertEquals("hello world", val);
        client.close();
    }

    @Test
    public void testApplicationContext() throws Exception {
        server.deploy(SimpleRestApp.class, "/root");
        Client client = ClientBuilder.newClient();
        String val = client.target(TestPortProvider.generateURL("/root/test"))
                .request().get(String.class);
        Assert.assertEquals("hello world", val);
        client.close();
    }

    @Test
    public void testDeploymentInfo() throws Exception {
        DeploymentInfo di = server.undertowDeployment(SimpleRestApp.class);
        di.setContextPath("/di");
        di.setDeploymentName("DI");
        server.deploy(di);
        Client client = ClientBuilder.newClient();
        String val = client.target(TestPortProvider.generateURL("/di/base/test"))
                .request().get(String.class);
        Assert.assertEquals("hello world", val);
        client.close();
    }
}
